package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.exception.RegistryException;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.repository.CuisineCustomerRepository;
import de.quandoo.recruitment.registry.repository.InMemoryCuisineCustomerRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final List<String> cuisineList = Arrays.asList("italian", "french", "german"); // TODO make it expandable

    private CuisineCustomerRepository pairing = new InMemoryCuisineCustomerRepository();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) throws RegistryException {
        validateCuisine(cuisine);
        validateCustomer(customer);
        pairing.put(customer.getUuid(), cuisine.getName());
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisine == null) {
            return Collections.emptyList();
        }
        return pairing.getCustomerNames(cuisine.getName())
                      .map(Customer::new)
                      .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customer == null) {
            return Collections.emptyList();
        }
        return pairing.getCuisineNames(customer.getUuid())
                      .map(Cuisine::new)
                      .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return pairing.getTopCuisineNames()
                      .map(Cuisine::new)
                      .limit(n)
                      .collect(Collectors.toList());
    }

    private void validateCustomer(Customer customer) throws RegistryException {
        if (customer == null || customer.getUuid() == null) {
            throw new RegistryException("Customer is null!");
        }
    }

    private void validateCuisine(Cuisine cuisine) throws RegistryException {
        if (cuisine == null || cuisine.getName() == null) {
            throw new RegistryException("Cuisine is null!");
        }
        if (!isKnownCuisine(cuisine.getName())) {
            throw new RegistryException("Unknown cuisine, please reach johny@bookthattable.de to update the code");
        }
    }

    private boolean isKnownCuisine(String cuisine) {
        return cuisineList.contains(cuisine);
    }
}
