package de.quandoo.recruitment.registry.repository;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.sun.tools.javac.util.Pair;

import java.util.stream.Stream;

public class InMemoryCuisineCustomerRepository implements CuisineCustomerRepository {

    private Multimap<String, String> customersByCuisine = HashMultimap.create();
    private Multimap<String, String> cuisineByCustomer  = HashMultimap.create();

    @Override
    public void put(String customer, String cuisine) {
        cuisineByCustomer.put(customer, cuisine);
        customersByCuisine.put(cuisine, customer);
    }

    @Override
    public Stream<String> getCustomerNames(String cuisine) {
        return cuisine == null ? Stream.empty() : customersByCuisine.get(cuisine).stream();
    }

    @Override
    public Stream<String> getCuisineNames(String customer) {
        return customer == null ? Stream.empty() : cuisineByCustomer.get(customer).stream();
    }

    @Override
    public Stream<String> getTopCuisineNames() {
        return customersByCuisine.keySet()
                                 .stream()
                                 .map(this::populatedCuisine)
                                 .sorted(this::compareCuisineSizesDesc)
                                 .map(it -> it.fst);
    }

    private Pair<String, Long> populatedCuisine(String it) {
        return Pair.of(it, (long) customersByCuisine.get(it).size());
    }

    private int compareCuisineSizesDesc(Pair<String, Long> cuisine0, Pair<String, Long> cuisine1) {
        return cuisine1.snd.compareTo(cuisine0.snd);
    }
}
