package de.quandoo.recruitment.registry.repository;

import java.util.stream.Stream;

public interface CuisineCustomerRepository {

    void put(String customerUid, String cuisineName);

    Stream<String> getCustomerNames(String cuisine);

    Stream<String> getCuisineNames(String customer);

    Stream<String> getTopCuisineNames();
}
