package de.quandoo.recruitment.registry.exception;

public class RegistryException extends RuntimeException {

    public RegistryException(String msg) {
        super(msg);
    }
}
