package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class GeneratedTests {

    @Test(timeout = 4000)
    public void test00() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Customer customer0 = new Customer("");
        Cuisine cuisine0 = new Cuisine("italian");
        inMemoryCuisinesRegistry0.register(customer0, cuisine0);
        List<Cuisine> list0 = inMemoryCuisinesRegistry0.topCuisines(1);
        assertFalse(list0.isEmpty());
    }

    @Test(timeout = 4000)
    public void test01() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Customer customer0 = new Customer("r^WGFn[4i");
        Cuisine cuisine0 = new Cuisine("italian");
        inMemoryCuisinesRegistry0.register(customer0, cuisine0);
        List<Cuisine> list0 = inMemoryCuisinesRegistry0.customerCuisines(customer0);
        assertFalse(list0.contains(cuisine0));
    }

    @Test(timeout = 4000)
    public void test02() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Customer customer0 = new Customer("r^WGFn[4i");
        Cuisine cuisine0 = new Cuisine("italian");
        inMemoryCuisinesRegistry0.register(customer0, cuisine0);
        List<Customer> list0 = inMemoryCuisinesRegistry0.cuisineCustomers(cuisine0);
        assertEquals(1, list0.size());
    }

    @Test(timeout = 4000, expected = RuntimeException.class)
    public void test03() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        // Undeclared exception!
        inMemoryCuisinesRegistry0.topCuisines((-1887));
        fail("Expecting exception: IllegalArgumentException");
    }

    @Test(timeout = 4000, expected = RuntimeException.class)
    public void test04() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Customer customer0 = new Customer(null);
        Cuisine cuisine0 = new Cuisine("italian");
        // Undeclared exception!
        inMemoryCuisinesRegistry0.register(customer0, cuisine0);
        fail("Expecting exception: NullPointerException");
    }

    @Test(timeout = 4000, expected = RuntimeException.class)
    public void test05() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Cuisine cuisine0 = new Cuisine("");
        inMemoryCuisinesRegistry0.register(null, cuisine0);
        fail("Expecting exception: RuntimeException");
    }

    @Test(timeout = 4000, expected = RuntimeException.class)
    public void test06() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Cuisine cuisine0 = new Cuisine("italian");

        inMemoryCuisinesRegistry0.register(null, cuisine0);
        fail("Expecting exception: RuntimeException");
    }

    @Test(timeout = 4000)
    public void test07() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        List<Cuisine> list0 = inMemoryCuisinesRegistry0.customerCuisines(null);
        assertEquals(0, list0.size());
    }

    @Test(timeout = 4000)
    public void test08() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Customer customer0 = new Customer("de.quandoo.recruitment.registry.InMemoryCuisinesRegistry");
        List<Cuisine> list0 = inMemoryCuisinesRegistry0.customerCuisines(customer0);
        assertEquals(0, list0.size());
    }

    @Test(timeout = 4000)
    public void test09() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        List<Customer> list0 = inMemoryCuisinesRegistry0.cuisineCustomers(null);
        assertEquals(0, list0.size());
    }

    @Test(timeout = 4000)
    public void test10() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Cuisine cuisine0 = new Cuisine("italian");
        List<Customer> list0 = inMemoryCuisinesRegistry0.cuisineCustomers(cuisine0);
        assertTrue(list0.isEmpty());
    }

    @Test(timeout = 4000, expected = RuntimeException.class)
    public void test11() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        Customer customer0 = new Customer("WV:E");
        inMemoryCuisinesRegistry0.register(customer0, null);
        fail("Expecting exception: RuntimeException");
    }

    @Test(timeout = 4000)
    public void test12() throws Throwable {
        InMemoryCuisinesRegistry inMemoryCuisinesRegistry0 = new InMemoryCuisinesRegistry();
        List<Cuisine> list0 = inMemoryCuisinesRegistry0.topCuisines(338);
        assertTrue(list0.isEmpty());
    }

}
