package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry registry = new InMemoryCuisinesRegistry();
    private Customer                 paris    = new Customer("Paris");
    private Customer                 berlin   = new Customer("Berlin");
    private Customer                 rome     = new Customer("Rome");
    private Customer                 bern     = new Customer("Bern");
    private Customer                 vienna   = new Customer("Vienna");

    private Cuisine french  = new Cuisine("french");
    private Cuisine german  = new Cuisine("german");
    private Cuisine italian = new Cuisine("italian");

    @Before
    public void initRegistry() {
        registry = new InMemoryCuisinesRegistry();

        registry.register(berlin, german);
        registry.register(vienna, german);
        registry.register(bern, german);

        registry.register(paris, french);
        registry.register(bern, french);

        registry.register(rome, italian);
        registry.register(bern, italian);
    }

    @Test
    public void registerSameCustomer() {
        List<Customer> frenchCustomers = registry.cuisineCustomers(french);
        assertEquals(2, frenchCustomers.size());
        registry.register(frenchCustomers.get(0), french);
        frenchCustomers = registry.cuisineCustomers(french);
        assertEquals(2, frenchCustomers.size());
    }

    @Test
    public void registerDifferentCustomer() {
        List<Customer> frenchCustomers = registry.cuisineCustomers(french);
        assertEquals(2, frenchCustomers.size());
        registry.register(new Customer("Algiers"), french);
        frenchCustomers = registry.cuisineCustomers(french);
        assertEquals(3, frenchCustomers.size());
    }

    @Test
    public void customersForNull() {
        assertEquals(Collections.emptyList(), registry.cuisineCustomers(null));
    }

    @Test
    public void customersForGerman() {
        List<Customer> customers = registry.cuisineCustomers(german);
        assertEquals(3, customers.size());
    }

    @Test
    public void cuisinesForNull() {
        assertEquals(Collections.emptyList(), registry.customerCuisines(null));
    }

    @Test
    public void cuisinesForBern() {
        List<Cuisine> cuisines = registry.customerCuisines(bern);
        assertEquals(3, cuisines.size());
    }

    @Test(expected = RuntimeException.class)
    public void registerUnknown() {
        Cuisine russian = new Cuisine("russian");
        registry.register(new Customer("Moscow"), russian);
        assert(false);
    }

    @Test
    public void topCuisinesTest() {
        List<Cuisine> cuisines = registry.topCuisines(2);
        assertEquals(2, cuisines.size());
        assertEquals(german.getName(), cuisines.get(0).getName());
        assertNotEquals(german.getName(), cuisines.get(1).getName());
    }

}
