# Cuisines Registry
## The solution
### What is done
1. I both generated automatically and has written some tests to make sure the existing code works fine (it didn't).
2. Fixed the bugs found on the step 1.
3. Reorganized the way to store cuisine-customer relations, so
    1. Easy to add a new cuisine: just add an element to cuisines array.
    2. No duplicated cuisine-customers registration are added.
    3. Make is possible for customers to follow more than one cuisine.
    4. Get cuisines by customer is done in O(1) time instead of O(n * m) times for n customers and m cuisines
    5. Get customers by cuisine is done in O(1) time as before (but without clumsiness of creating a new lists for it)
    6. Get top cuisines can be implemented to work in O(1) as well.
4. Implemented get top cuisines function.
### What isn't done
1. I was not allowed to change the interface, but if I were, I would
    1. Disallow using nullable cuisines and customers (by using @NotNull annotation or null-safe kotlin type)
    2. Speaking of kotlin, I would use inline classes to wrap Strings up instead of containers classes used now
    3. Add exceptions in the methods declaration to make it explicit.    
### What's next
1. To make cuisines list expandable i would add a method to add a new cuisine in the list. So we will be able to do it on the fly.
2. All the read methods are implemented to work in constant time, but the write method `register` is not, 
but it does not give a feedback to the user, so it can be done asynchronously.
